<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$loginId	=	$_SESSION['LogID'];
$loginType	=	$_SESSION['LogType'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch($optype)
{
	// NEW SECTION
	case 'index':
						$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
						$db->connect();
				
						$data['heading'] 		 =	$App->convert($_REQUEST['heading']);
						$data['description']	 =	$App->convert($_REQUEST['description']);
						
						$image_name = $_FILES['image']['name'];
						$ext = pathinfo($image_name, PATHINFO_EXTENSION);
						$next	=	date("ymdhis");
						$data['image'] = $image_name;
						if($data['image']!='')
						{
							$data['image'] = 'offer'.$next.'.'.$ext;
						}
						else
						{
							$data['image']='';
						}
						$sourcePath = $_FILES['image']['tmp_name'];
						$targetPath = "../../../images/new_offer/" . $data['image'];
						move_uploaded_file($sourcePath, $targetPath);

						$success1=$db->query_insert(TABLE_OFFERS,$data);
									
						$db->close();
						
							if($success1)
							{							
								$_SESSION['msg']="New Offer Added Successfully";										
							}
							else{
								$_SESSION['msg']="Failed";
							}
							header("location:index.php");
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];
		if(!$_REQUEST['heading'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=0;
								
										
						$data['heading']			=	$App->convert($_REQUEST['heading']);
						$data['description']		=	$App->convert($_REQUEST['description']);
						$image_name = $_FILES['image']['name'];
						$ext = pathinfo($image_name, PATHINFO_EXTENSION);
						$next	=	date("ymdhis");
						$data['image'] = $image_name;
						if($data['image']!='')
						{
							$data['image'] = 'offer'.$next.'.'.$ext;
						}
						else
						{
							$data['image']='';
						}
						
						if($image_name)
                        {
							
							// Deleting old image
							
							$tableEditQry	=  "SELECT *						  
												  FROM ".TABLE_OFFERS."						  
												 WHERE ".TABLE_OFFERS.".ID='$editId'";
							
							$tableEdit 	=	mysql_query($tableEditQry);
							$editRow	=	mysql_fetch_array($tableEdit); 
											
							// End
							

							$delete_image = $editRow['image'];
							
							$file= ("../../../images/new_offer/".$delete_image);
 							unlink($file);
							$sourcePath = $_FILES['image']['tmp_name'];
							$targetPath = "../../../images/new_offer/".$data['image'];
							move_uploaded_file($sourcePath,$targetPath);
						}
						else
						{
							$data['image'] = $editRow['image'];
						}
						

						$success1=$db->query_update(TABLE_OFFERS,$data," ID='{$editId}'");
									
						$db->close();
						
							if($success1)
							{							
								$_SESSION['msg']= "Updated Successfully";										
							}
							else{
								$_SESSION['msg']="Failed";
							}
									
				header("location:index.php");
														
			}	
		break;	
				
	case 'delete':
	
			$id = $_REQUEST['id'];	
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);	
			$db->connect();	
			// Deleting old image
							
					$tableEditQry	=  "SELECT *						  
										FROM ".TABLE_OFFERS."						  
										WHERE ".TABLE_OFFERS.".ID='$id'";
					
					$tableEdit 	=	mysql_query($tableEditQry);
					$editRow	=	mysql_fetch_array($tableEdit); 
											
			// End
			$delete_image = $editRow['image'];
							
			$file= ("../../../images/new_offer/".$delete_image);
 			unlink($file);
			
			$db->query("DELETE FROM `".TABLE_OFFERS."` WHERE ID='{$id}'");								
			$db->close(); 
			
			$_SESSION['msg']="Deleted Successfully";					
			header("location:index.php");
						
		break;
}
?>