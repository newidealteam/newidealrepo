<?php 
$page_id=2;
include("../header.php"); 
	
?>
<?php
//echo $_SESSION['LogID'];
if(!isset($_SESSION['LogID']))
{
//echo "hi";
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

?>
        <!--end header-->
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    <?php
					if(isset($_SESSION['msg']))
					{
						if($_SESSION['msg']=='')
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:none">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
                        <?php 
						} 
						else
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:block">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
						<?php 
						} ?>
                        
                    <?php 
					}	
					$_SESSION['msg']='';
					?>
                        <h1 class="page-header">
                          <small> New Offers </small> <a href="#" data-toggle="modal" data-target="#myModal1" style="float:right" class="btn btn-primary">Add New</a>
                        </h1>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover offer_table">
                                <thead>
                                    <tr>
                                    	<th>Sl/N</th>
                                        <th>Heading</th>
                                        <th>Description</th>
                                        <th>Image</th>
                                        <th width="110">Edit / Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
									$i=0;
									$selectAll = "select  * from ".TABLE_OFFERS."";
									$result = $db->query($selectAll);
									$number	=	mysql_num_rows($result);
									if(mysql_num_rows($result)==0)
									{
									?>
                                        <tr><td colspan="5" align="center">There is no data in list. </td></tr>
									<?php
									}
									else
									{
										/*********************** for pagination ******************/
									$rowsPerPage = ROWS_PER_PAGE;
									if(isset($_GET['page']))
									{
										$pageNum = $_GET['page'];
									}
									else
									{
										$pageNum =1;
									}
									$offset = ($pageNum - 1) * $rowsPerPage;
									$select2=$db->query($selectAll." limit $offset, $rowsPerPage");
									//echo $select2;
									$i=$offset+1;
									//use '$select1' for fetching
									/*************************** for pagination **************/
										
										while ($row = mysql_fetch_array($select2)) 
										{
											$tableId = $row['id'];
										?>
										<tr>
											<td><?= $i++; ?></td>
											<td><?= $row['heading']; ?></td>
											<td><?= $row['description']; ?></td>
											<td><?= $row['image']; ?>
                                            <?php 
                                            if($row['image']!='')
                                            {
											?>
                                            	<a href="#" style="display:block" data-src="<?= $row['image']; ?>" data-target="#myModal" class="image_view">view</a>
                                            <?php
                                            }
											else
											{
											?>
												<a href="#" style="" data-src="dummy.jpg" data-target="#myModal" class="image_view">view</a>
											<?php
											}
											?>
                                            </td>
                                            

											<td>
											<a onclick="return confirm('Do you want to delete this package?');" href="do.php?op=delete&id=<?= $tableId ?>" style="float:right;" class="btn btn-primary"><i class="fa fa-remove"></i></a>
											<a href="edit.php?id=<?= $tableId ?>" style="float:right;margin-right:10px" class="btn btn-primary"><i class="fa fa-edit"></i></a>
											</td>
										</tr>
										<?php
								   		}
									}
                    			?>
                                </tbody>
                            </table>
                        </div>
                        
                        <!-- paging -->		
            <!--<div style="clear:both;"></div>
            <div class="text-center">
                <div class="btn-group pager_selector"></div>
            </div> -->       
            <!-- paging end-->
            
            <!--*****************************************************************-->
            
            	 <?php 
                  if($number>@$rowsPerPage)
					{
					?>	
					 <br />	
					  <div class="pagerSC" align="center">
					<?php
					
					$query   =  $db->query($selectAll);
					$numrows = mysql_num_rows($query);
					$maxPage = ceil($numrows/$rowsPerPage);
					$self = $_SERVER['PHP_SELF'];
					$nav  = '';
					if ($pageNum - 5 < 1) {
					$pagemin = 1;
					} else {
					$pagemin = $pageNum - 5;
					};
					if ($pageNum + 5 > $maxPage) {
					$pagemax = $maxPage;
					} else {
					$pagemax = $pageNum + 5;
					};
					
					for($page = $pagemin; $page <= $pagemax; $page++)
					{
					   if ($page == $pageNum)
					   {
						  $nav .= " <span class=\"currentSC\">$page</span> "; // no need to create a link to current page
					   }
					   else
					   {
						 	if(@$search)
					   		 {
							 	$nav .= " <a href=\"$self?page=$page&sname=$search\">$page</a> ";
							 }
							 else
							 {
							 	$nav .= " <a href=\"$self?page=$page\">$page</a> ";
							 }
					   }
					}
					?>
					 <?php
					if ($pageNum > 1)
					{
					   $page  = $pageNum - 1;
					   if(@$search)
					   {
						   $prev  = " <a href=\"$self?page=$page&sname=$search\">Prev</a> ";
						   $first = " <a href=\"$self?page=1&sname=$search\">First Page</a> ";
					   }
					   else
					   {
						   $prev  = " <a href=\"$self?page=$page\">Prev</a> ";
						   $first = " <a href=\"$self?page=1\">First Page</a> ";
					   }
					}
					else
					{
					   $prev  = '&nbsp;';
					   $first = '&nbsp;';
					}
					
					if ($pageNum < $maxPage)
					{
					   $page = $pageNum + 1;
					   if(@$search)
					   {
						   	 $next = " <a href=\"$self?page=$page&sname=$search\">Next</a> ";
						     $last = " <a href=\"$self?page=$maxPage&sname=$search\">Last Page</a> ";
					   }
					   else
					   {
						   	 $next = " <a href=\"$self?page=$page\">Next</a> ";
						   	 $last = " <a href=\"$self?page=$maxPage\">Last Page</a> ";
					   }
					}
					else
					{
					   $next = '&nbsp;';
					   $last = '&nbsp;';
					}
					echo $first . $prev . $nav . $next . $last;
					?>
					<div style="clear: left;"></div>
					</div>	 
				<?php
				}
                ?>
            
           <!-- ******************************************************************-->
                        
                        
                        
                        
                        
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
    </div>
    <!-- add new_ideas-->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content" style="overflow:auto;padding-bottom:20px">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add New Offers</h4>
              </div>
              <div class="modal-body">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Enqry" style="height:auto;margin-top:5px; padding:0;">
                    
                    <form role="form" method="post" enctype="multipart/form-data" action="do.php?op=index">
                    <div style="float:none !important;margin:0 auto;">
                            <div class="form-group">
                                <label>Heading</label>
                                <input type="text" name="heading" class="form-control" required>
                            </div>                            
                            <div class="form-group">
                                <label>Choose image (798 * 582)</label>
                                <input name="image" type="file">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" style="max-width:100%;min-height:145px;" class="form-control" rows="3" required></textarea>
                            </div>
                            <button style="float:right" type="submit" class="btn btn-default" name="submit">Submit</button>
                            <button style="float:right; margin-right:10px" type="reset" class="btn btn-default">Reset</button>
                    </div>                    
                    </form>
                    </div>
              </div>
            </div>
          </div>
    </div>
    <!-- Ends add new_ideas-->




    <!-- image view-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" style="margin-top:-9px; outline:none" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<!--                <h4 class="modal-title" id="myModalLabel">Heading</h4>-->
              </div>
              <div class="modal-body">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style=" height:auto;margin-top:5px; padding:0;">
                        <img style="width:100%" src="">
                    </div>
              </div>
              <div class="modal-footer" style="display: inline-block;width: 100%;">
              </div>
            </div>
          </div>
    </div>
    <!-- image view-->




    
    <!-- jQuery -->
    <script src="../../js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>
    <!-- Morris Charts JavaScript -->
    
    <script>
		$(document).ready(function() {
            // Dynamic images in pop up
			$(document).on('click', '.offer_table > tbody > tr > td a[data-src]', function (e) {
				e.preventDefault();
				var imgSrc = $(this).attr('data-src');
				$('#myModal').find('.modal-body img').attr('src', '../../../images/new_offer/' + imgSrc);		
				$('#myModal').modal('show');	
			});
			
        });
	</script>
    
    
    <script src="../../js/plugins/morris/raphael.min.js"></script>
    <script src="../../js/plugins/morris/morris.min.js"></script>
    <script src="../../js/plugins/morris/morris-data.js"></script>
</body>
</html>
