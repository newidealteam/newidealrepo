<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$loginId	=	$_SESSION['LogID'];
$loginType	=	$_SESSION['LogType'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch($optype)
{
	// NEW SECTION
	case 'index':
						$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
						$db->connect();
				
						$data['productName'] 	 =	$App->convert($_REQUEST['productName']);
						$data['subCategoryId']	 =	$App->convert($_REQUEST['subId']);
						$data['categoryId']		 =	$App->convert($_REQUEST['category']);
						$data['description']	 =	$App->convert($_REQUEST['description']);
						
						$image_name = $_FILES['image']['name'];
						$ext = pathinfo($image_name, PATHINFO_EXTENSION);
						$next	=	date("ymdhis");
						$data['image'] = $image_name;
						if($data['image']!='')
						{
							$data['image'] = 'product'.$next.'.'.$ext;
						}
						else
						{
							$data['image']='';
						}
						$sourcePath = $_FILES['image']['tmp_name'];
						$targetPath = "../Products/productPhoto/" . $data['image'];
						move_uploaded_file($sourcePath, $targetPath);
						//print_r($data);die;
						$success1=$db->query_insert(TABLE_PRODUCTS,$data);
									
						$db->close();
						
							if($success1)
							{							
								$_SESSION['msg']="New Product Added Successfully";										
							}
							else{
								$_SESSION['msg']="Failed";
							}
							header("location:index.php");
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];
		if(!$_REQUEST['productName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=0;
								
										
						$data['productName'] 	 =	$App->convert($_REQUEST['productName']);
						$data['subCategoryId']	 =	$App->convert($_REQUEST['subId']);
						$data['categoryId']		 =	$App->convert($_REQUEST['category']);
						$data['description']	 =	$App->convert($_REQUEST['description']);
						
						$image_name = $_FILES['image']['name'];
						$ext = pathinfo($image_name, PATHINFO_EXTENSION);
						$next	=	date("ymdhis");
						$data['image'] = $image_name;
						if($data['image']!='')
						{
							$data['image'] = 'product'.$next.'.'.$ext;
						}
						else
						{
							$data['image']='';
						}
						
						if($image_name)
                        {
							
							// Deleting old image
							
							$tableEditQry	=  "SELECT *						  
												  FROM ".TABLE_PRODUCTS."						  
												 WHERE ".TABLE_PRODUCTS.".ID='$editId'";
							
							$tableEdit 	=	mysql_query($tableEditQry);
							$editRow	=	mysql_fetch_array($tableEdit); 
							//echo $tableEditQry;die;				
							// End
							

							$delete_image = $editRow['image'];
							
							$file= ("../Products/productPhoto/" .$delete_image);
 							unlink($file);
							$sourcePath = $_FILES['image']['tmp_name'];
							$targetPath = "../Products/productPhoto/" .$data['image'];
							move_uploaded_file($sourcePath,$targetPath);
						}
						else
						{
							$data['image'] = $editRow['image'];
						}
						
						//print_r($data);die;
						$success1=$db->query_update(TABLE_PRODUCTS,$data," ID='{$editId}'");
									
						$db->close();
						
							if($success1)
							{							
								$_SESSION['msg']= "Updated Successfully";										
							}
							else{
								$_SESSION['msg']="Failed";
							}
									
				header("location:index.php");
														
			}	
		break;	
				
	case 'delete':
	
			$id = $_REQUEST['id'];	
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);	
			$db->connect();	
			// Deleting old image
							
					$tableEditQry	=  "SELECT *						  
										FROM ".TABLE_PRODUCTS."						  
										WHERE ".TABLE_PRODUCTS.".ID='$id'";
					
					$tableEdit 	=	mysql_query($tableEditQry);
					$editRow	=	mysql_fetch_array($tableEdit); 
											
			// End
			$delete_image = $editRow['image'];
							
			$file= ("../Products/productPhoto/".$delete_image);
 			unlink($file);
			
			$db->query("DELETE FROM `".TABLE_PRODUCTS."` WHERE ID='{$id}'");								
			$db->close(); 
			
			$_SESSION['msg']="Deleted Successfully";					
			header("location:index.php");
						
		break;
}
?>