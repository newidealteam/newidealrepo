<?php 
$page_id=3;
include("../header.php"); ?>
<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$editId=$_REQUEST['id'];
	$tableEditQry	=  "SELECT *						  
						  FROM ".TABLE_PRODUCTS."						  
						 WHERE ".TABLE_PRODUCTS.".ID='$editId'";
	
	$tableEdit 	=	mysql_query($tableEditQry);
	$editRow	=	mysql_fetch_array($tableEdit);
	$catId		=	$editRow['categoryId'];

?>
<script>
function getSubcategory()
{
category=document.getElementById('category').value;

var xmlhttp;
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("subId").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","getSubcategory.php?category="+category,true);
xmlhttp.send();
}
</script>
<div id="page-wrapper">
          <div class="container-fluid">
          
              <form action="do.php?op=edit" class="form1" method="post" enctype="multipart/form-data"> <!-- onsubmit="return valid()" -->
			  <input type="hidden" name="id" id="id" value="<?php echo $editId ?>"> 		  
               
			  <div class="row">
				   <div class="col-lg-12">
                    <?php
					if(isset($_SESSION['msg']))
					{
						if($_SESSION['msg']=='')
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:none">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
                        <?php 
						} 
						else
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:block">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
						<?php 
						} ?>
                        
                    <?php 
					}	
					$_SESSION['msg']='';
					?>
                        <h1 class="page-header">
                          <small>Edit Products</small> <a href="index.php" style="float:right" class="btn btn-primary"> < Back </a>
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-fw fa-table"></i>  <a href="index.php">Products</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-edit"></i>Edit Products
                            </li>
                        </ol>
                    </div>                  
                    <div class="col-lg-6" style="float:none !important;margin:0 auto;">
                    
                    		<div class="form-group">
                                <label>Prodcut Name</label>
                                <input type="text" id="productName" name="productName" class="form-control" required="" value="<?= $editRow['productName']; ?>">
                            </div>
                            
                            <div class="form-group">
                            	<label>Category</label>
                             <select name="category" class="admin_inner" id="category" onchange="getSubcategory()">
                            	<option value="">--Select category--</option>
                            	<?php
		                      	$select = mysql_query("select * from ".TABLE_CATEGORY."");
		                      	while($row=mysql_fetch_array($select))
		                      	{
		                      		//$catId = $row['ID'];
		                      		//echo $catId;
								?>
								<option value="<?php echo $row['ID'];?>" <?php if($row['ID']==$editRow['categoryId']){ echo 'selected' ;}?>><?php echo $row['categoryName'];?></option>
								<?php
								}
		                      	?>
                            </select>
                            </div> 
                            
                            <!--for Ajax of sub category -->   
                            <div class="form-group">
							Sub Category:</br> 
							  <select class="admin_inner" name="subId" id="subId" required="required">
							  
							  	<?php
							  	$select2="select ".TABLE_SUBCATEGORY.".ID,".TABLE_SUBCATEGORY.".subCategoryName from ".TABLE_SUBCATEGORY." where ".TABLE_SUBCATEGORY.".categoryId='$catId' order by ".TABLE_SUBCATEGORY.".subCategoryName";
							  	$res2=mysql_query($select2);
							  	while($row2=mysql_fetch_array($res2))
							  	{
							  	?>
								<option value="<?php echo $row2['ID'];?>" <?php if($row2['ID']==$editRow['subCategoryId']){ echo 'selected' ;}?>><?php echo $row2['subCategoryName'];?></option>
									<?php
									}
									?>					
							  </select>
							</div> 
							<!--end-->
                                                       
                            <div class="form-group">
                                <label>Description</label>
                                <textarea name="description" style="max-width:100%;min-height:145px;" class="form-control" rows="3"><?= $editRow['description']; ?></textarea>
                            </div>
                            
                            <div class="form-group">
                                <label>Choose image (798 * 582)</label>
                                <input name="image" type="file">
                            </div>
                            
                            <input style="float:right;" type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
                            
                      </div>                    
             	 	</div>
                 </form>                 
             </div>
         </div>      
  	  <div>
  </div> 
      <!-- jQuery -->
    <script src="../../js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>
    <!-- Morris Charts JavaScript -->
    
    <script>
		$(document).ready(function() {
            // Dynamic images in pop up
			$(document).on('click', '.offer_table > tbody > tr > td a[data-src]', function (e) {
				e.preventDefault();
				var imgSrc = $(this).attr('data-src');
				$('#myModal').find('.modal-body img').attr('src', '../../../images/new_offer/' + imgSrc);		
				$('#myModal').modal('show');	
			});
			
        });
	</script>
    
    
    <script src="../../js/plugins/morris/raphael.min.js"></script>
    <script src="../../js/plugins/morris/morris.min.js"></script>
    <script src="../../js/plugins/morris/morris-data.js"></script>
</body>
</html>