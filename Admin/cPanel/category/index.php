<?php include("../header.php"); ?>
<?php
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<!--end header-->
        <div id="page-wrapper">
            <div class="container-fluid">
                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                    <?php
					if(isset($_SESSION['msg']))
					{
						if($_SESSION['msg']=='')
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:none">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
                        <?php 
						} 
						else
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:block">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
						<?php 
						} ?>
                        
                    <?php 
					}	
					$_SESSION['msg']='';
					?>
                        <h1 class="page-header">
                          <small> Category </small> 
                        </h1>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover offer_table">
                                <thead>
                                    <tr>
                                    	<th>SLNO</th>
                                        <th>Category Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
									$selectAll = "select  * from ".TABLE_CATEGORY."";
									$result = $db->query($selectAll);
									while($row = mysql_fetch_array($result))
									{
								?>	
										<tr>
											<td><?= $row['ID']; ?></td>
											<td><?= $row['categoryName']; ?></td>
                                        </tr>   
                    			<?php } 
                    			?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>