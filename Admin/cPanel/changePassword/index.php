<?php
$page_id = "";
include("../header.php");
//echo $_SESSION['LogID'];
if(!isset($_SESSION['LogID']))
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();



?>
<script>
function valid()
{

flag=false;

jPassword=document.getElementById('newPassword').value;
jConfirmPassword=document.getElementById('conPassword').value;		
	if(jConfirmPassword=="" || jConfirmPassword!=jPassword)
	{																			///for password
	document.getElementById('pwddiv').innerHTML="Password And Confirm Password are Not Equal";
	flag=true;
	}

if(flag==true)
	{
		return false;
	}

}

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

</script>
<div id="page-wrapper">
<div class="container-fluid">
<div class="row">

<?php
					if(isset($_SESSION['msg']))
					{
						if($_SESSION['msg']=='')
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:none">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
                        <?php 
						} 
						else
						{
						?>
                            <div class="alert alert-success alert-dismissible" role="alert" style="display:block">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo $_SESSION['msg']; ?>
                            </div>
						<?php 
						} ?>
                        
                    <?php 
					}	
					$_SESSION['msg']='';
					?>
					
			<h1 class="page-header">
                          <small> Change Password </small> 
                        </h1>
	<div class="col-lg-6 col-md-6 col-sm-6" style="float:none !important; margin:0 auto ">
		<div class="bd_panel bd_panel_default bd_panel_shadow">
			<form method="post" action="do.php?op=index" class="default_form" onsubmit=" return valid()">
				
				<div class="bd_panel_body">
					<div class="panel_row">
						<div class="panel_col">
							<label>Existing User name</label>
						</div>
						<div class="panel_col">
							<input type="text" class="form-control" autocomplete="off" name="oldUserName" id="oldUserName" required="">
						</div>
					</div>
					<div class="panel_row">
						<div class="panel_col">
							<label>Existing Password</label>
						</div>
						<div class="panel_col">
							<input autocomplete="new-password" class="form-control" type="password" name="oldPassword" id="oldPassword" required="" >
						</div>
					</div>
					<div class="panel_row">
						<div class="panel_col">
							<label>New Password</label>
						</div>
						<div class="panel_col">
							<input type="password" class="form-control" name="newPassword" id="newPassword" required="">
						</div>
					</div>
					<div class="panel_row">
						<div class="panel_col">
							<label>Confirm Password</label>
						</div>
						<div class="panel_col">
							<input type="password" class="form-control" name="conPassword" id="conPassword" required="" onfocus="clearbox('pwddiv')">
							 <div id="pwddiv" class="valid" style="color:#FF9900;"></div>
						</div>
					</div>
				</div>
				<div class="bd_panel_footer">
					<div class="panel_row">
						<div class="panel_col_full">
							<input class="btn btn-primary" style="margin-top:20px; float:right; width:100px" type="submit" name="form" value="SAVE">
						</div>
					</div>
				</div>
				
			</form>
		</div>
	</div>
</div>
</div>
</div>
   <!-- jQuery -->
    <script src="../../js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.min.js"></script>
    <!-- Morris Charts JavaScript -->
    
    <script>
		$(document).ready(function() {
            // Dynamic images in pop up
			$(document).on('click', '.offer_table > tbody > tr > td a[data-src]', function (e) {
				e.preventDefault();
				var imgSrc = $(this).attr('data-src');
				$('#myModal').find('.modal-body img').attr('src', '../../../images/new_offer/' + imgSrc);		
				$('#myModal').modal('show');	
			});
			
        });
	</script>
    
    
    <script src="../../js/plugins/morris/raphael.min.js"></script>
    <script src="../../js/plugins/morris/morris.min.js"></script>
    <script src="../../js/plugins/morris/morris-data.js"></script>
</body>
</html>