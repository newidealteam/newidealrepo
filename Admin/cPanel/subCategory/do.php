<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$loginId	=	$_SESSION['LogID'];
$loginType	=	$_SESSION['LogType'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
switch($optype)
{
	// NEW SECTION
	case 'index':
	
			if(!$_REQUEST['category'])
			{
				$_SESSION['msg']="Error, Invalid Details!";			
				header("location:index.php");		
			}
				
		else
			{
						$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
						$db->connect();
				
						$category	 				=	$App->convert($_REQUEST['category']);
						$subCategory	 			=	$App->convert($_REQUEST['subCategory']);
						
						$data['categoryId']	 		=	$App->convert($_REQUEST['category']);
						$data['subCategoryName']	=	$App->convert($_REQUEST['subCategory']);	
						//print_r($data);die;
						$success1=$db->query_insert(TABLE_SUBCATEGORY,$data);
									
						$db->close();
						
							if($success1)
							{							
								$_SESSION['msg']="New Sub Category Added Successfully";										
							}
							else{
								$_SESSION['msg']="Failed";
							}
							header("location:index.php");
					}
		break;		
	// EDIT SECTION
	case 'edit':		
		$editId	=	$_REQUEST['id'];
		if(!$_REQUEST['category'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:index.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success1=0;
								
						
						$category	 				=	$App->convert($_REQUEST['category']);
						$subCategory	 			=	$App->convert($_REQUEST['subCategory']);			
						$data['categoryId']	 		=	$App->convert($_REQUEST['category']);
						$data['subCategoryName']	=	$App->convert($_REQUEST['subCategory']);	
						
						$success1=$db->query_update(TABLE_SUBCATEGORY,$data," ID='{$editId}'");
									
						$db->close();
						
							if($success1)
							{							
								$_SESSION['msg']= "Updated Successfully";										
							}
							else{
								$_SESSION['msg']="Failed";
							}
									
				header("location:index.php");
														
			}	
		break;	
				
	case 'delete':
	
			$id = $_REQUEST['id'];	
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE);	
			$db->connect();	
			
			$db->query("DELETE FROM `".TABLE_SUBCATEGORY."` WHERE ID='{$id}'");								
			$db->close(); 
			$_SESSION['msg']="Deleted Successfully";					
			header("location:index.php");
						
		break;
}
?>