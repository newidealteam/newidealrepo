<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>New Ideal Ceramic Admin-Login</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.css" rel="stylesheet">
    <link href="css/elegant-icons-style.css" rel="stylesheet" />
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="css/sb-admin.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
<style>
html{background:url(images/login_bg.jpg) no-repeat center center fixed;-webkit-background-size:cover;-moz-background-size:cover;-o-background-size:cover;background-size:cover;}
.input-group input{background:rgba(0,0,0,0.5) !important;border:1px solid #404040 !important;font-family:arial;font-size:13px !important;}
.input-group input:focus{border:1px solid #1d5693 !important; color:#1d5693}
.btn-primary{color:#fff !important;background-color:#1d5693 !important;border:1px solid #1d5693 !important;font-family:arial;font-size:13px !important;}
.btn-primary:hover{background:rgba(0,0,0,0) !important;}
.error_msg{ text-align:center; background:#F00; color:#fff; font-family:Arial;width:auto !important; margin:20px auto;}
</style>
</head>
  <body class="login-img3-body">
    <div class="container">
      <form class="login-form" method="post" action="do.php">
<?php
session_start();
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
            
        <div class="login-wrap">
            <p class="login-img"><img style="width:100%; padding-bottom:20px" src="../images/logo.jpg"></p>
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
              <input type="text" name="userName" id="userName" class="form-control" placeholder="Username" value="<?php if (@isset($_COOKIE['username'])) 
	{echo $_COOKIE['username']; }?>">
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" name="password" id="password" class="form-control" placeholder="Password">
            </div>
            <button name="login" class="btn btn-primary btn-lg btn-block" type="submit">Login</button>
 </div>
      </form>
    </div>
  </body>
</html>