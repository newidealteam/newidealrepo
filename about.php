<?php $page_id=2; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Ideal Ceramic Co. W.L.L</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <style>.header_main{margin-top:0px;box-shadow: 10px 10px 5px #888888;}.footer_main{ margin-top:0px}.navbar{ margin-bottom:0}</style>
</head>
<body>
	<!--start header-->	
	<?php include('includes/header.php'); ?>
	<!--end header-->
	<div class="banner_inner"><img src="images/about.jpg"></div>
<!--    <div class="shadow"><div class="container"><img src="images/shadow.png"></div></div>
--><!--start contact_content-->
	<div class="container">
        <div class="col-xs-12 contact_head">ABOUT US</div>
        <div class="col-xs-12 contact_underline"><img src="images/undrln.jpg"></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom:20px;margin-top:30px;">
                <p style="text-align:justify;font-size:14px;line-height:26px;">
                	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.<br><br>
                    It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                </p>
            </div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 about_left">
         	<div class="col-xs-12 contact_head_sub">Why chose us ?</div>
            <ul class="wlcome_itm" style="color:#000;">
                 <li><i class="fa fa-check-square-o" aria-hidden="true"></i> &nbsp; New KG System</li>
                 <li><i class="fa fa-check-square-o" aria-hidden="true"></i> &nbsp; Talent Hub</li>
                 <li><i class="fa fa-check-square-o" aria-hidden="true"></i> &nbsp; High potential care(HPC) and Low potential Care (LPC)</li>
                 <li><i class="fa fa-check-square-o" aria-hidden="true"></i> &nbsp; soft skill & Personality Lab</li>
                 <li><i class="fa fa-check-square-o" aria-hidden="true"></i> &nbsp; Outdoor Class Area</li>
                 <li><i class="fa fa-check-square-o" aria-hidden="true"></i> &nbsp; Effective Evaluation and Reporting</li>
            </ul>
		 </div>
         <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 about_left">
         	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission">
            	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 mission_icon"><i class="fa fa-rocket" aria-hidden="true"></i></div>
                <div class="col-lg-10 col-md-9 col-sm-10 col-xs-10 mission_content">
                	<h3>Our Mission</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mission">
            	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 mission_icon"><i class="fa fa-eye" aria-hidden="true"></i></div>
                <div class="col-lg-10 col-md-9 col-sm-10 col-xs-10 mission_content">
                	<div><h3>Our Vision</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p></div>
                </div>
            </div>
		 </div>
        </div>
    </div> 
    
<!--ends contact_content-->	
<!--start footer-->
	<?php include('includes/footer.php'); ?>
	<!--ends footer-->
    <!-- End header_main -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
