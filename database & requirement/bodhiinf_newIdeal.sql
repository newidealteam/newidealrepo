-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 31, 2017 at 05:19 AM
-- Server version: 5.5.54-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bodhiinf_newIdeal`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `ID` int(11) NOT NULL,
  `categoryName` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`ID`, `categoryName`) VALUES
(1, 'Tiles'),
(2, 'Sanitary'),
(3, 'Aluminium'),
(4, 'Steel/Stainless Steel');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `ID` int(11) NOT NULL,
  `userName` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`ID`, `userName`, `password`) VALUES
(1, 'Admin', 'd14555f58f01bb48501c42bd463706a0');

-- --------------------------------------------------------

--
-- Table structure for table `newideas`
--

CREATE TABLE `newideas` (
  `id` int(20) NOT NULL,
  `roomid` int(20) NOT NULL,
  `heading` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newideas`
--

INSERT INTO `newideas` (`id`, `roomid`, `heading`, `image`) VALUES
(4, 1, 'white', 'idea160801122816.jpg'),
(10, 1, 'black', 'idea160801122834.jpg'),
(11, 2, 'royal', 'idea160801122413.jpg'),
(12, 2, 'platinum', 'idea160801122539.jpg'),
(18, 3, 'cures', 'idea160801123142.jpg'),
(19, 3, 'hedges', 'idea160801123509.jpg'),
(20, 3, 'hedges', 'idea160801123510.jpg'),
(21, 3, 'hedges', 'idea160801123510.jpg'),
(22, 4, 'hytr', 'idea160801123846.jpg'),
(23, 4, 'royal seniorita', 'idea160801123927.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `newoffers`
--

CREATE TABLE `newoffers` (
  `id` int(20) NOT NULL,
  `heading` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newoffers`
--

INSERT INTO `newoffers` (`id`, `heading`, `description`, `image`) VALUES
(2, 'sale for free', 'for customers purchasing more then 20000', 'offer160801124046.jpg'),
(4, 'big sale', '50% off for living room set', 'offer160801124547.jpg'),
(6, 'super sale', 'sdhbmnbndb\r\nG\r\n\r\ng\r\ne\r\ng\r\n\r\ner\r\ng\r\ne\r\n\r\ng\r\ne\r\ng\r\ne\r\n\r\ng\r\n\r\n\r\n\r\n\r\n\r\ne\r\ng\r\ne\r\n\r\nhfgjhfgjhfjfd\r\nj\r\nfd\r\nj\r\nf\r\ndj\r\n\r\nfd\r\nj\r\nf\r\ndj\r\nf\r\nj\r\n\r\nfd\r\njjkhdfghdjkghjdhgjkdhkjg\r\n\r\nd\r\ng\r\nd\r\n\r\ngd', 'offer160801013915.jpg'),
(7, 'saale sales', 'thesis for sale..\r\nfor today only..\r\nstocks limited', 'offer160801014047.jpg'),
(8, 'gsg', 'ghafahsgfhja..\r\najdabjfaj\r\n....ashajfhjka\r\ngahsjgdahjg...', 'offer160801014137.jpg'),
(10, 'xcvx', 'xvc', ''),
(11, 'ffg', 'fdgfggdfgdf', '');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ID` int(11) NOT NULL,
  `productName` varchar(50) NOT NULL,
  `subCategoryId` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ID`, `productName`, `subCategoryId`, `categoryId`, `description`, `image`) VALUES
(5, '', 0, 0, '', ''),
(6, 'tile sample1', 3, 1, 'sample1', 'product160801125157.jpg'),
(7, 'tile sample2', 3, 1, 'new', 'product160801125235.jpg'),
(8, 'tile sample3', 7, 1, 'new tile', 'product160801125316.jpg'),
(9, 'sanitary sample1', 4, 2, 'new', 'product160801125342.jpg'),
(10, 'sanitary sample2', 10, 2, 'new2', 'product160801125404.jpg'),
(11, 'sample sanitary3', 11, 2, 'new', 'product160801125436.jpg'),
(12, 'aluminium', 5, 3, 'new', 'product160801125556.jpg'),
(13, 'aluminium sample2', 13, 3, 'new sample', 'product160801125635.jpg'),
(14, 'aluminium sample3', 5, 3, 'sdfsdf', 'product160801125831.jpg'),
(15, 'steel sample1', 6, 4, 'new', 'product160801125904.jpg'),
(16, 'steel sample2', 6, 4, 'new', 'product160801125927.jpg'),
(17, 'steel sample3', 6, 4, 'new', 'product160801010011.jpg'),
(19, 'rte', 13, 3, 'erbbb', 'product160801010316.jpg'),
(22, 'ert', 10, 2, 'ertfdd', 'product160801010818.jpg'),
(29, 'test sample without image', 8, 1, 'new', ''),
(30, 'tile', 8, 1, 'zxxzzxxz', 'product170530040652.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `roomtype`
--

CREATE TABLE `roomtype` (
  `id` int(20) NOT NULL,
  `room` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roomtype`
--

INSERT INTO `roomtype` (`id`, `room`) VALUES
(1, 'Living Room'),
(2, 'Bed Room'),
(3, 'Kitchen'),
(4, 'Bath Room');

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `ID` int(11) NOT NULL,
  `categoryId` int(11) NOT NULL,
  `subCategoryName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcategory`
--

INSERT INTO `subcategory` (`ID`, `categoryId`, `subCategoryName`) VALUES
(3, 1, 'sample tile1'),
(4, 2, 'sample sanitary1'),
(5, 3, 'sample aluminium1'),
(6, 4, 'sample steel1'),
(7, 1, 'marinate'),
(8, 1, 'italian'),
(9, 1, 'rajasthan'),
(10, 2, 'rtyyr'),
(11, 2, 'rttt'),
(13, 3, 'qwsdeee'),
(15, 4, 'pk'),
(16, 4, 'tata'),
(18, 4, 'kairali txt');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `newideas`
--
ALTER TABLE `newideas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newoffers`
--
ALTER TABLE `newoffers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `roomtype`
--
ALTER TABLE `roomtype`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `newideas`
--
ALTER TABLE `newideas`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `newoffers`
--
ALTER TABLE `newoffers`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `roomtype`
--
ALTER TABLE `roomtype`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
