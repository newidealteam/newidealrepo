<?php

require("Admin/config/config.inc.php"); 
require("Admin/config/Database.class.php");
require("Admin/config/Application.class.php");

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>


<div class="header_main">
            <nav class="navbar navbar-inverse" role="navigation">
                <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header col-lg-4 col-md-4 col-sm-3 col-xs-12">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-9">
                        <a class="navbar-brand" href="#" style="padding:0;height:auto;">
                            <img style="width:100%;height:56px;" src="images/logo.jpg" alt="">
                        </a>
                        </div>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12" id="menu_mainrk">
                    <div class="navbar-collapse collapse" id="bs-example-navbar-collapse-1" aria-expanded="false" style="height: 1px;">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php" <?php if($page_id==1) echo'class=active';?> >Home</a></li>
                            <li><a href="about.php" <?php if($page_id==2) echo'class=active';?> >About</a></li>
                            <li><a href="new_offers.php" <?php if($page_id==3) echo'class=active';?> >New offer</a></li>
                            <li><a href="product.php" <?php if($page_id==4) echo'class=active';?> >Product</a></li>
                            <li><a href="new_ideas.php" <?php if($page_id==5) echo'class=active';?> >New ideas</a></li>
                            <li><a href="contact.php" <?php if($page_id==6) echo'class=active';?> >Contact</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container -->
            </nav>
        </div>