<?php
$page_id=1; 
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Ideal Ceramic Co. W.L.L</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="js/amazingslider-1.css">
    <link rel="stylesheet" type="text/css" href="js/initcarousel-1.css">
    <style type="text/css">body, html {height: 100%;margin: 0;padding: 0;overflow: hidden;}
    .amazingslider-text-1{left: 35px !important; text-align: left !important;}
	.amazingslider-text-box-1{ z-index:99 !important;position:absolute;top:338px;width:100%;}
	.amazingslider-text-wrapper-1{ width:55% !important;}
	.amazingslider-title-1{width:90% !important;font-family:roboto !important; }
	.amazingslider-arrow-left-1,.amazingslider-arrow-right-1{ display:none !important}
    </style>
</head>
<body>
	<!-- Start full slider -->
    <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:100%;height:100%;margin:0 auto;">
        <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
            <ul class="amazingslider-slides" style="display:none;">
                <li>
                	<img src="images/a1.jpg" title="Lorem  Ipsum is simply  dummy text the printing." />
					<button class="as-btn-orange-medium">Read More</button>
                </li>
<!--                <li>
                	<img src="images/a2.jpg" title="Lorem  Ipsum is simply  dummy text the printing" />
					<button class="as-btn-orange-medium">Read More</button>
                </li>
-->            </ul>
        </div>
    </div>
    <!-- End full slider -->
    <div class="main_wapper">
        <?php include('includes/header.php'); ?>
        <div class="right_part">
            <ul>
                <a href="#"><li>Download <br>Catalogue</li></a>
                <a href="new_offers.php"><li>Special <br>Offers</li></a>
                <a href="#" data-toggle="modal" data-target="#myModal"><li>Online <br>Enquiry</li></a>
            </ul>
        </div>
         <?php $catQuery1 = "SELECT * FROM ".TABLE_CATEGORY."";
                	  $selectcatAll1= $db->query($catQuery1);
                	  $num1=0;
            		  $result1		=	mysql_num_rows($selectcatAll1);
            		  while($catRows1 = mysql_fetch_array($selectcatAll1))
            		  {
						$editRow1[$num1][1] = $catRows1['categoryName'];
						$editRow1[$num1][2] = $catRows1['ID'];
						$num1++;
					  }	
                ?>
        <div class="new_arrivals">
        	<div class="container">
            	<div class="col-lg-5 col-md-6 col-sm-7 col-xs-10" id="new_arrivals1">
                    <div id="amazingcarousel-container-1">
                        <div id="amazingcarousel-1" style="display:none;position:relative;width:100%;max-width:420px;margin:0px auto 0px;">
                            <div class="amazingcarousel-list-container">
                                <ul class="amazingcarousel-list">
                                
                             <?php $proQuery1	=	"SELECT * FROM ".TABLE_PRODUCTS."
                             						 WHERE  ".TABLE_PRODUCTS.".categoryId =1";  
                             						 
                             						 $select= $db->query($proQuery1); 
                             						 //echo $proQuery1;
                             	$productNum		=	mysql_num_rows($select);					 							
                             	if($productNum==0)
                             	{
									?>
                                   
                                    <?php
								}
                             	
                             	else {
                             	
                             	
                             	while($editRow	=	mysql_fetch_array($select))
								{			 
                                
                                ?>
                                    <li class="amazingcarousel-item">
                                        <div class="amazingcarousel-item-container">
                                            <div class="amazingcarousel-image">
                                            <?php if($editRow['image']!='')
                                            {
												
											?>
                                                <a href="Admin/cPanel/Products/productPhoto/<?= $editRow['image']; ?>"  class="html5lightbox" data-group="amazingcarousel-1">
                                                    <img src="Admin/cPanel/Products/productPhoto/<?= $editRow['image']; ?>"  alt="rk1" />
                                                </a>
                                                <?php } else { ?>
													<a href="Admin/cPanel/Products/productPhoto/dummy.jpg"  class="html5lightbox" data-group="amazingcarousel-1">
                                                    <img src="Admin/cPanel/Products/productPhoto/dummy.jpg"  alt="rk1" />
                                                </a>
											<?php	} ?>
                                            </div>
                                       </div>
                                    </li>
                            <?php } }?>
                                </ul>
                                <div class="amazingcarousel-prev"></div>
                                <div class="amazingcarousel-next"></div>
                            </div>
                            <div class="amazingcarousel-nav"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-7 col-xs-10" id="new_arrivals2" style="visibility:hidden">
                    <div id="amazingcarousel-container-2">
                        <div id="amazingcarousel-1" style="display:none;position:relative;width:100%;max-width:420px;margin:0px auto 0px;">
                            <div class="amazingcarousel-list-container">
                            <ul class="amazingcarousel-list">
                            <?php $proQuery2	=	"SELECT * FROM ".TABLE_PRODUCTS."
                             						 WHERE  ".TABLE_PRODUCTS.".categoryId =2";  
                             						 
                             						 $select1= $db->query($proQuery2); 
                             						 //echo $proQuery1;
                             	$productNum1		=	mysql_num_rows($select1);					 						
                             	if($productNum1==0)
                             	{
									?>
                                   
                                    <?php
								}
                             	
                             	else {
									
                             		
                             	while($editRow2	=	mysql_fetch_array($select1))
								{			 
                                
                                ?>
                            
                                    <li class="amazingcarousel-item">
                                        <div class="amazingcarousel-item-container">
                                            <div class="amazingcarousel-image">
                                            <?php if($editRow2['image']!='')
                                            {
												
											?>
                                                <a href="Admin/cPanel/Products/productPhoto/<?= $editRow2['image']; ?>"  class="html5lightbox" data-group="amazingcarousel-1">
                                                    <img src="Admin/cPanel/Products/productPhoto/<?= $editRow2['image']; ?>"  alt="rk1" />
                                                </a>
                                                <?php } else { ?>
                                                <a href="Admin/cPanel/Products/productPhoto/dummy.jpg"  class="html5lightbox" data-group="amazingcarousel-1">
                                                    <img src="Admin/cPanel/Products/productPhoto/dummy.jpg"  alt="rk1" />
                                                </a>
													
											<?php	} ?>
                                            </div>
                                       </div>
                                    </li>
                                   <?php } } ?> 
                                   </ul>
                                <div class="amazingcarousel-prev"></div>
                                <div class="amazingcarousel-next"></div>
                            </div>
                            <div class="amazingcarousel-nav"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-7 col-xs-10" id="new_arrivals3"  style="visibility:hidden">
                    <div id="amazingcarousel-container-2">
                        <div id="amazingcarousel-1" style="display:none;position:relative;width:100%;max-width:420px;margin:0px auto 0px;">
                            <div class="amazingcarousel-list-container">
                                <ul class="amazingcarousel-list">
                                
                                <?php $proQuery3	=	"SELECT * FROM ".TABLE_PRODUCTS."
                             						 WHERE  ".TABLE_PRODUCTS.".categoryId =3";  
                             						 
                             						 $select2= $db->query($proQuery3); 
                             						 //echo $proQuery1;
                             	$productNum2		=	mysql_num_rows($select2);	
                             	//echo $productNum2;
                             	if($productNum2==0)
                             	{
									?>
                                    <?php
								}
                             	
                             	else {
									
												 							
                             	while($editRow3	=	mysql_fetch_array($select2))
								{			 
                                
                                ?>
                                    <li class="amazingcarousel-item">
                                        <div class="amazingcarousel-item-container">
                                            <div class="amazingcarousel-image">
                                            <?php if($editRow3['image']!='')
                                            { ?>
												
											
                                                <a href="Admin/cPanel/Products/productPhoto/<?= $editRow3['image']; ?>"  class="html5lightbox" data-group="amazingcarousel-1">
                                                    <img src="Admin/cPanel/Products/productPhoto/<?= $editRow3['image']; ?>"  alt="rk1" />
                                                </a>
                                                <?php } else { ?>
                                                <a href="Admin/cPanel/Products/productPhoto/dummy.jpg"  class="html5lightbox" data-group="amazingcarousel-1">
                                                    <img src="Admin/cPanel/Products/productPhoto/dummy.jpg"  alt="rk1" />
                                                </a>
													
											<?php	} ?>
                                            </div>
                                       </div>
                                    </li>
                                   <?php } }?> 
                                </ul>
                                <div class="amazingcarousel-prev"></div>
                                <div class="amazingcarousel-next"></div>
                            </div>
                            <div class="amazingcarousel-nav"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 col-sm-7 col-xs-10" id="new_arrivals4"  style="visibility:hidden">
                    <div id="amazingcarousel-container-2">
                        <div id="amazingcarousel-1" style="display:none;position:relative;width:100%;max-width:420px;margin:0px auto 0px;">
                            <div class="amazingcarousel-list-container">
                                <ul class="amazingcarousel-list">
                                
                                 <?php $proQuery4	=	"SELECT * FROM ".TABLE_PRODUCTS."
                             						 WHERE  ".TABLE_PRODUCTS.".categoryId =4";  
                             						 
                             						 $select4= $db->query($proQuery4); 
                             						 //echo $proQuery1;
                             	$productNum4		=	mysql_num_rows($select4);	
                             	//echo $productNum2;
                             	if($productNum4==0)
                             	{
									?>
                                    <?php
								}
                             	
                             	else {
									
												 							
                             	while($editRow4	=	mysql_fetch_array($select4))
								{			 
                                
                                ?>
                                    <li class="amazingcarousel-item">
                                        <div class="amazingcarousel-item-container">
                                            <div class="amazingcarousel-image">
                                            <?php if($editRow4['image']!='')
                                            {
												?>
											
                                                <a href="Admin/cPanel/Products/productPhoto/<?= $editRow4['image']; ?>"  class="html5lightbox" data-group="amazingcarousel-1">
                                                    <img src="Admin/cPanel/Products/productPhoto/<?= $editRow4['image']; ?>"  alt="rk1" />
                                                </a>
                                               <?php } else { ?>
                                               <a href="Admin/cPanel/Products/productPhoto/dummy.jpg"  class="html5lightbox" data-group="amazingcarousel-1">
                                                    <img src="Admin/cPanel/Products/productPhoto/dummy.jpg"  alt="rk1" />
                                                </a>
											   	
											 <?php  } ?>
                                            </div>
                                       </div>
                                    </li>
                               <?php } } ?>     
                                </ul>
                                <div class="amazingcarousel-prev"></div>
                                <div class="amazingcarousel-next"></div>
                            </div>
                            <div class="amazingcarousel-nav"></div>
                        </div>
                    </div>
                </div>
                              
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding:0">
                    <div class="col-lg-1 col-md-1 hidden-sm hidden-xs new_arrivals_icon"></div>
                    
                   
                    <div onClick="granite();" class="col-lg-2 col-md-2 col-sm-2 col-xs-12 new_arrivals_icon_bottn new_arrivals_icon_bottn_first new_arrivals_icon_active" id="new_arrvl"><?php  echo $editRow1[0][1]; ?></div>
                    <div class="col-lg-1 col-md-1 hidden-sm hidden-xs new_arrivals_icon"></div>
                    
                    
                    
                    <div onClick="import_marble();" class="col-lg-2 col-md-2 col-sm-3 col-xs-12 new_arrivals_icon_bottn" id="new_arrv2"><?php  echo $editRow1[1][1]; ?></div>
                    <div class="col-lg-1 col-md-1 hidden-sm hidden-xs new_arrivals_icon"></div>
                    	
                    <div onClick="stone();" class="col-lg-2 col-md-2 col-sm-3 col-xs-12 new_arrivals_icon_bottn" id="new_arrv3"><?php  echo $editRow1[2][1];  ?></div>
                    <div class="col-lg-1 col-md-1 hidden-sm hidden-xs new_arrivals_icon"></div>
                    <div onClick="import_granite();" class="col-lg-3 col-md-2 col-sm-3 col-xs-12 new_arrivals_icon_bottn" id="new_arrv4"><?php  echo $editRow1[3][1];  ?></div>
                    
                </div>
            </div>
        </div>
        
        <!-- Online Enquiry -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Online Enquiry</h4>
              </div>
              <div class="modal-body">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 Enqry" style=" height:auto;margin-top:5px; padding:0;">
                        <form id="contact_form" method="post" action="">
                            <input type="text" name="name" placeholder="Full Name *" required>
                            <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" placeholder="Email *"  required>
                            <input type="text" name="contact_number" pattern="[0-9]{10}" placeholder="Contact Number *" required>
                            <input type="text" name="subject" placeholder="Subject *" />
                            <textarea name="message" placeholder="Comments"></textarea>
                            <!--<div class="button_capcha">
                                <div id='captchaimg'>
                                <a href="#" title="Refresh"><img style="float:left" src="images/captcha.jpg"  alt="CAPTCHA Image" ></a>
                                </div>
                               <a href="" >
                               <div class="refrsh"><img style="width:40px" src="images/refresh.png"></div>
                               </a>
                            </div>  
                            <input class="cont_sbmt" style="height:38px;margin:0px;float:left" type="text" name="" placeholder="Captcha Code" />--> 
                            <input class="more1" style="margin-top:0px;cursor:pointer;margin-left:10px" type="reset" name="" value="Clear" >
							<input class="more1" style="margin-top:0px;cursor:pointer" type="submit" name="submit" value="Submit" >
                         </form>
                         <?php   
							if(isset($_POST["submit"]))
							{
								$name=$_POST['name'];
								$email=$_POST['email'];   
								$message=$_POST['message'];
								$subjct=$_POST['subject'];
								$contact=$_POST['contact_number'];                      
								$recipient = "alisha.bodhi@gmail.com";
								$subject = "New Ideal Ceramic - Online Enquiry: Contact Added successfully";
								$message = "Name : .$name.\n\nEmail: $email\n\nSuject : $subjct\n\nContact Number : $contact\n\nMessage : $message";
								$header="From:$email\r\n";
								$header.="Reply-To:$email\r\n";
								$header.="MIME-Version:1.0\r\n";
								'X-Mailer: PHP/' . phpversion();
								
								$s=mail($recipient,$subject,$message, $header);
								if($s)
									{
										echo('Your message has been send');
									}
									else
									{
										echo('Error On Sending, Please Try Again Later');
									}
							}
						?>
                    </div>
              </div>
              <div class="modal-footer" style="display: inline-block;width: 100%;">
              </div>
            </div>
          </div>
        </div>
        <!--End Online Enquiry --> 

        
        
        <!--footer-->
            <?php include('includes/footer.php'); ?>
        <!--ends footer-->
	<!--<div style="width:100px; height:400px; background:#966"></div>-->
    </div>
    <!-- End header_main -->
    <script>
    	function granite()
		{
			/*alert('granite')*/
			document.getElementById('new_arrivals1').style.visibility="visible";
			document.getElementById('new_arrivals2').style.visibility="hidden";
			document.getElementById('new_arrivals3').style.visibility="hidden";
			document.getElementById('new_arrivals4').style.visibility="hidden";
			//document.getElementById('new_arrivals5').style.visibility="hidden";
			var element1 = document.getElementById("new_arrvl");
				element1.classList.add("new_arrivals_icon_active");
			var element2 = document.getElementById("new_arrv2");
				element2.classList.remove("new_arrivals_icon_active");
			var element3 = document.getElementById("new_arrv3");
				element3.classList.remove("new_arrivals_icon_active");
			var element4 = document.getElementById("new_arrv4");
				element4.classList.remove("new_arrivals_icon_active");
			//var element5 = document.getElementById("new_arrv5");
				//element5.classList.remove("new_arrivals_icon_active");
		}
    	function import_marble()
		{
			/*alert('import_marble')*/
			document.getElementById('new_arrivals1').style.visibility="hidden";
			document.getElementById('new_arrivals2').style.visibility="visible";
			document.getElementById('new_arrivals3').style.visibility="hidden";
			document.getElementById('new_arrivals4').style.visibility="hidden";
			//document.getElementById('new_arrivals5').style.visibility="hidden";
			var element1 = document.getElementById("new_arrvl");
				element1.classList.remove("new_arrivals_icon_active");
			var element2 = document.getElementById("new_arrv2");
				element2.classList.add("new_arrivals_icon_active");
			var element3 = document.getElementById("new_arrv3");
				element3.classList.remove("new_arrivals_icon_active");
			var element4 = document.getElementById("new_arrv4");
				element4.classList.remove("new_arrivals_icon_active");
			//var element5 = document.getElementById("new_arrv5");
				//element5.classList.remove("new_arrivals_icon_active");
		}
    	function stone()
		{
			/*alert('stone')*/
			document.getElementById('new_arrivals1').style.visibility="hidden";
			document.getElementById('new_arrivals2').style.visibility="hidden";
			document.getElementById('new_arrivals3').style.visibility="visible";
			document.getElementById('new_arrivals4').style.visibility="hidden";
			//document.getElementById('new_arrivals5').style.visibility="hidden";
			var element1 = document.getElementById("new_arrvl");
				element1.classList.remove("new_arrivals_icon_active");
			var element2 = document.getElementById("new_arrv2");
				element2.classList.remove("new_arrivals_icon_active");
			var element3 = document.getElementById("new_arrv3");
				element3.classList.add("new_arrivals_icon_active");
			var element4 = document.getElementById("new_arrv4");
				element4.classList.remove("new_arrivals_icon_active");
			//var element5 = document.getElementById("new_arrv5");
				//element5.classList.remove("new_arrivals_icon_active");
		}
    	function import_granite()
		{
			/*alert('import_granite')*/
			document.getElementById('new_arrivals1').style.visibility="hidden";
			document.getElementById('new_arrivals2').style.visibility="hidden";
			document.getElementById('new_arrivals3').style.visibility="hidden";
			document.getElementById('new_arrivals4').style.visibility="visible";
			//document.getElementById('new_arrivals5').style.visibility="hidden";
			var element1 = document.getElementById("new_arrvl");
				element1.classList.remove("new_arrivals_icon_active");
			var element2 = document.getElementById("new_arrv2");
				element2.classList.remove("new_arrivals_icon_active");
			var element3 = document.getElementById("new_arrv3");
				element3.classList.remove("new_arrivals_icon_active");
			var element4 = document.getElementById("new_arrv4");
				element4.classList.add("new_arrivals_icon_active");
			//var element5 = document.getElementById("new_arrv5");
				//element5.classList.remove("new_arrivals_icon_active");
		}
    	
    </script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/amazingslider.js"></script>
    <script src="js/initslider-1.js"></script>
    <script src="js/amazingcarousel.js"></script>
    <script src="js/initcarousel-1.js"></script>

<?php require("footer.php"); ?>