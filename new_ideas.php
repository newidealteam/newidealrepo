<?php $page_id=5;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Ideal Ceramic Co. W.L.L</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <style>.header_main{margin-top:0px;box-shadow: 10px 10px 5px #888888;}.footer_main{ margin-top:0px}.navbar{ margin-bottom:0}
	.gallery_item_content h4{ font-size:15px !important}</style>
    <!-- Start galley slider -->
	<link rel="stylesheet" href="index_files/vlb_files1/vlightbox1.css" type="text/css" />
	<link rel="stylesheet" href="index_files/vlb_files1/visuallightbox.css" type="text/css" media="screen" />
    <!--pagination-->
	<link rel="stylesheet" href="css/jquery.paginate.css" />
</head>
<body>
	<!--start header-->	
	<?php include('includes/header.php'); ?>
	<!--end header-->
	<div class="banner_inner"><img src="images/newidea.jpg"></div>
<!--start contact_content-->
	<div class="container" style="margin-bottom:30px">
    	<div class="row">
            <div class="col-xs-12 contact_head">NEW IDEAS</div>
            <div class="col-xs-12 contact_underline"><img src="images/undrln.jpg"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inner_main">
                	<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12 new_idea_categories">
                    	<ul>
                        	<?php
								$selectAll = "select  * from ".TABLE_ROOMTYPE;
								$result = $db->query($selectAll);
								$room_typeid=1;
							if(@$_REQUEST['room_typeid'])
							{
							$room_typeid = $_REQUEST['room_typeid'];
							
							}
								while ($row = mysql_fetch_array($result)) 
								{
							?>
                        	<li><a href="new_ideas.php?room_typeid=<?= $row['id'] ?>" <?php if($row['id']==$room_typeid){ echo "class='active'";} ?>><?= $row['room'] ?><div class="arrow-right"></div></a></li>
                            <!--<li><a href="#">Bed Room<div class="arrow-right"></a></li>
                            <li class="active"><a href="#">Kitchen<div class="arrow-right"></a></li>
                            <li><a href="#">Bath Room<div class="arrow-right"></a></li>-->
							<?php } ?> 
                        </ul>
                    </div>
                	<div class="col-lg-10 col-md-9 col-sm-8 col-xs-12" style="text-align:center;">
                        <!--Start Gallery Slider-->                    
                        <div id="vlightbox1">
                        <ul id="example">
                        <?php 
							$selectAll = "select  * from ".TABLE_IDEAS." WHERE ".TABLE_IDEAS.".roomid='$room_typeid'";
							$result = $db->query($selectAll);
							if(mysql_num_rows($result)==0)
							{
								echo "There is no data in list";
							}
							else
							{
								while ($row = mysql_fetch_array($result)) 
								{
									$id = $row['id'];
									$room_id = $row['roomid'];
									switch($room_id)
									{
										case "1":
											$folderName = "living_room";
											break;
										case "2":
											$folderName = "bed_room";
											break;
										case "3":
											$folderName = "kitchen";
											break;
										case "4":
											$folderName = "bath_room";
											break;
										default;
									}
                                                        if($row['image']!='')
									{
						?>
                     
						
                            <a class="vlightbox1" id="g_item_main" href="<?php echo "images/new_ideas/".$folderName."/".$row['image'] ?>" title="kitchen-1">
                                <img style="width:216px; height:162px" src="<?php echo "images/new_ideas/".$folderName."/".$row['image'] ?>"/>
                                <div class="gallery_item_content">
                                    <h4><?= $row['heading']; ?></h4>
                                    
                                </div>
                            </a>
                                   <?php } else { ?>
                                   <a class="vlightbox1" id="g_item_main" href="images/new_ideas/dummy.jpg" title="kitchen-1">
                                <img style="width:216px; height:162px" src="images/new_ideas/dummy.jpg"/>
                                <div class="gallery_item_content">
                                    <h4><?= $row['heading']; ?></h4>
                                    
                                </div>
                            </a>
                            
                        <?php  
                                                                   }         
								}
						    } 
					    ?>
						</ul>
                        </div>
                        <!--Ends Gallery Slider-->
                	</div>
            	</div><!--Ends inner_main-->
		</div>
        <!--Read more popup --
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Lorem Ipsum</h4>
              </div>
              <div class="modal-body">
                   <p style="text-align:justify; line-height:26px; padding:10px">
                   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever 
                   since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only 
                   five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with 
                   the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus 
                   PageMaker including versions of Lorem Ipsum.
                   </p>     
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>-->
        <!--End Read more popup --> 
    </div> 
<!--ends contact_content-->	
<!--start footer-->
	<?php include('includes/footer.php'); ?>
<!--ends footer-->
<!-- End header_main -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
<!--    <script src="index_files/vlb_engine/jquery.min.js" type="text/javascript"></script>-->
	<script src="index_files/vlb_engine/visuallightbox.js" type="text/javascript"></script>
    <script src="index_files/vlb_engine/thumbscript1.js" type="text/javascript"></script>
	<script src="index_files/vlb_engine/vlbdata1.js" type="text/javascript"></script>
    	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>-->
        	<script src="js/jquery.paginate.js"></script>
	
	<script>	
		//call paginate
		$('#example').paginate({
			
		});
	</script>


</body>
</html>