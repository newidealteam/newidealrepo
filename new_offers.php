<?php $page_id=3; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Ideal Ceramic Co. W.L.L</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery.paginate.css" />
    <style>.header_main{margin-top:0px;box-shadow: 10px 10px 5px #888888;}.footer_main{ margin-top:0px}.navbar{ margin-bottom:0}</style>
</head>
<body>
	<!--start header-->	
	<?php include('includes/header.php'); ?>
	<!--end header-->
	<div class="banner_inner"><img src="images/offer.jpg"></div>
<!--    <div class="shadow"><div class="container"><img src="images/shadow.png"></div></div>
--><!--start contact_content-->
	<div class="container" style="margin-bottom:30px;">
    	<div class="row">
            <div class="col-xs-12 contact_head">NEW OFFERS</div>
            <div class="col-xs-12 contact_underline"><img src="images/undrln.jpg"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inner_main">
                <ul id="example">
                	
                	<?php $newOffQuery	=	"SELECT * FROM ".TABLE_OFFERS."";
                		  //echo $newOffQuery;
                		  $selectcatAll= $db->query($newOffQuery);
                		  $result		=	mysql_num_rows($selectcatAll);
                		  
                		  if($result==0)
		                		  	{
										?><h4> There is No New Offers! </h4><?php
									}
                		  else 
                		  {	
                		  
                		  while($offerRows = mysql_fetch_array($selectcatAll))
								{
									$offerId = $offerRows['id'];
								?>
                	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 offer_items">
                    	    <?php if($offerRows['image']!='')
                    		{ ?>
								<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 offer_items_image"><img src="images/new_offer/<?php echo $offerRows['image']; ?>"></div><?php
							} else { ?>
								<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 offer_items_image"><img src="images/new_offer/dummy.jpg"></div>
							<?php }?>
                        	
                            <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 offer_items_content">
                                <h3><?php echo $offerRows['heading']; ?></h3>
                                <p>
                                	<?php echo(substr($offerRows['description'], 0, 50)); ?>...
                                </p>     
                                <a href="#" data-toggle="modal" data-target="#myModal<?=$offerRows['id']?>">Read More &raquo;</a>                       
                            </div>
                        </div>
                    
	                    <div class="modal fade" id="myModal<?=$offerRows['id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			      <div class="modal-dialog" role="document">
			        <div class="modal-content">
			          <div class="modal-header">
			            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			            <h4 class="modal-title" id="myModalLabel"><?php echo $offerRows['heading']; ?></h4>
			          </div>
			          <div class="modal-body">
			          
			               <p style="text-align:justify; line-height:26px; padding:10px">
			               <?= $offerRows['description']; ?>
			               </p>     
			          </div>
			          
			          <div class="modal-footer">
			            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			          </div>
			        </div>
			      </div>
			    </div>
                    </div>
                    
                    <?php                 
                    
                    }
                    }
                    ?>
                    </ul>
                </div>
                
                
                
    

                
                
                
            </div>
		</div>            
    </div> 
    
    <!-- Read more popup -->
    
    
    <!--End Read more popup --> 
       
<!--ends contact_content-->	
<!--start footer-->
	<?php include('includes/footer.php'); ?>
	<!--ends footer-->
    <!-- End header_main -->
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
    <script src="js/jquery.paginate.js"></script>
	
	<script>	
		//call paginate
		$('#example').paginate({
			
		});
	</script>
    </body>
</html>