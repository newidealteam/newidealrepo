<?php $page_id=4; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Ideal Ceramic Co. W.L.L</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery.paginate.css" />
    <style>.header_main{margin-top:0px;box-shadow: 10px 10px 5px #888888;}.footer_main{ margin-top:0px}.navbar{ margin-bottom:0}</style>
    <!-- Accordion Menu -->
    <link rel="stylesheet" href="css/style.css">
    <!-- galley slider -->
	<link rel="stylesheet" href="index_files/vlb_files1/vlightbox1.css" type="text/css" />
	<link rel="stylesheet" href="index_files/vlb_files1/visuallightbox.css" type="text/css" media="screen" />
</head>
<body>
	<!--start header-->	
	<?php include('includes/header.php'); ?>
	<!--end header-->
	<div class="banner_inner"><img src="images/product.jpg"></div>
<!--start contact_content-->
	<div class="container" style="margin-bottom:30px">
    	<div class="row">
            <div class="col-xs-12 contact_head">OUR PRODUCTS</div>
            <div class="col-xs-12 contact_underline"><img src="images/undrln.jpg"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 inner_main">
                	<div class="col-lg-2 col-md-3 col-sm-4 col-xs-12" style="padding:0">
                    <ul class="mainmenu">
                    
                    <?php	$proQuery	= "SELECT * FROM ".TABLE_CATEGORY."";
                    					   //echo $proQuery;
                    	  $selectcatAll= $db->query($proQuery);
                		  $result		=	mysql_num_rows($selectcatAll);
                		  while($offerRows = mysql_fetch_array($selectcatAll))
                		  
                		  { 
                		  $catId= $offerRows['ID'];
                		  ?>
						 			   
                            <li><span><?= $offerRows['categoryName']; ?><span></li>
                                <ul class="submenu">
                                <div class="expand-triangle"><img src="images/expand.png"></div>
                        <?php $subquery = "SELECT ".TABLE_SUBCATEGORY.".ID,
            					   		      ".TABLE_SUBCATEGORY.".subCategoryName 
            					   		      FROM ".TABLE_SUBCATEGORY." 
            					   		      WHERE ".TABLE_SUBCATEGORY.".categoryId='$catId'";
                          $selectSub= $db->query($subquery);
                		  $result2		=	mysql_num_rows($selectSub);
                		  while($subRows = mysql_fetch_array($selectSub))
                		  
                		  { 
                		  $subCatID = $subRows['ID'];
                    					   		      
                                ?>
                                    
                                    <a href="product.php?id=<?= $subCatID; ?>"><li><span><?php echo $subRows['subCategoryName'];  ?></span></li></a>
                                    
                           <?php  }  ?>
                                    
                                </ul>
                            <?php } ?>	
						
						
                        </ul>
                       
                    </div>
                	<div class="col-lg-10 col-md-9 col-sm-8 col-xs-12" style="text-align:center;">
                        <!--Start Gallery Slider-->                    
                        <div id="vlightbox1">
                        <ul id="example">
                        <?php 
                        $cond	= 1;
                        
                        if(@$_REQUEST['id'])
                        {
						$subID		=	$_REQUEST['id'];
						$cond = $cond. " and ".TABLE_PRODUCTS.".subCategoryId ='$subID'"; 						}
                        
                        $subID=@$_REQUEST['id'];
                        
                        $productQry = "SELECT ".TABLE_PRODUCTS.".ID,
                        							".TABLE_PRODUCTS.".productName,
                        							".TABLE_PRODUCTS.".description,
                        							".TABLE_PRODUCTS.".image
                        					FROM ".TABLE_PRODUCTS."
                        					WHERE $cond";
                        					
                        		//echo $productQry;			
                        					$proAll	= $db->query($productQry);
                        					$resultQ		=	mysql_num_rows($proAll);
                        					
                        					
                		  
                		  if($resultQ==0)
		                		  	{
										?><h4> There is no product in this category..! </h4><?php
									}
                		  else 
                		  {		
                		  	
                		  
                		  	while($proRows = mysql_fetch_array($proAll))
                		
                		  	 	{
							 	
                		  		$proId = $proRows['ID'];
                                                if($proRows['image']!='')
                                            {
                		  		
                		  ?>
                            <a class="vlightbox1" id="g_item_main" href="Admin/cPanel/Products/productPhoto/<?= $proRows['image']; ?>" title="<?= $proRows['productName']; ?>">
                                <img style="width:216px; height:162px" src="Admin/cPanel/Products/productPhoto/<?= $proRows['image']; ?>"/>
                                <div class="gallery_item_content">
                                    <h4><?= $proRows['productName']; ?></h4>
                                    <p><?php echo(substr($proRows['description'], 0, 20)); ?>..</p>
                                </div>
                            </a>
                             <?php
                                            }
											else
											{
											?>
									<a class="vlightbox1" id="g_item_main" href="Admin/cPanel/Products/productPhoto/dummy.jpg" title="<?= $proRows['productName']; ?>">
                                <img style="width:216px; height:162px" src="Admin/cPanel/Products/productPhoto/dummy.jpg"/>
                                <div class="gallery_item_content">
                                    <h4><?= $proRows['productName']; ?></h4>
                                    <p><?php echo(substr($proRows['description'], 0, 20)); ?>..</p>
                                </div>
                            </a>
                          
       <?php 
       }
       } //end of while 
       }//end of else
       ?>
       					</ul>
       					</div>                 
                 
                	</div>
            	</div><!--Ends inner_main-->
		</div>
        <!-- Read more popup -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Lorem Ipsum</h4>
              </div>
              <div class="modal-body">
                   <p style="text-align:justify; line-height:26px; padding:10px">
                   Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever 
                   since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only 
                   five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with 
                   the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus 
                   PageMaker including versions of Lorem Ipsum.
                   </p>     
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <!--End Read more popup --> 
    </div> 
<!--ends contact_content-->	
<!--start footer-->
	<?php include('includes/footer.php'); ?>
<!--ends footer-->
<!-- End header_main -->
    <script src="js/jquery.min.js"></script>
    
	<!--<script src="index_files/vlb_engine/jquery.min.js" type="text/javascript"></script>-->
    <!-- galley slider -->
	<script src="index_files/vlb_engine/visuallightbox.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="index_files/vlb_engine/thumbscript1.js" type="text/javascript"></script>
	<script src="index_files/vlb_engine/vlbdata1.js" type="text/javascript"></script>
    <!-- Accordation menu -->
    <script src="js/script.js"></script>
	<script src="js/retina.min.js"></script>
	
	<script src="js/jquery.paginate.js"></script>
	
	<script>	
		//call paginate
		$('#example').paginate();
	</script>
</body>
</html>